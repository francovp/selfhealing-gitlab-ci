> :warning: **Experimental repository**: This is an experimental repository for research and educational purposes only, use at your own risk. I do not take any liability for any damage caused by this repository.

# selfhealing-action-express
This is a GitLab fork of the Calvin Hoenes's [GitHub - selfhealing-action-express](https://github.com/xpluscal/selfhealing-action-express).  I only added .gitlab-ci.yml (and removed the GitHub Actions) to trigger GitLab CI/CD pipeline instead of the GitHub Actions workflow.  I will update this repository as additional updates are made to this self healing CI pipeline process.

Demo experimental repository of a self-healing code in GitLab CI/CD pipeline using [Langchain](https://github.com/hwchase17/langchain) and [Openai](https://openai.com/).

The GitLab CI/CD pipeline is language agnostic and works on single file errors only for now.

## What is GitLab?
GitLab is a complete DevSecOps solution that does all from Project Planning and Source Code Management to CI/CD, Monitoring, and Security.  Parts of the README has been updated to reflect GitLab side of things.  For a free 30-Day trial to experience full GitLab Ultimate features, if you are interested, please email min.choi@canarimedia.com 

## What is this?
The repository is a simple express typescript server with a GitLab CI/CD pipeline that triggers the healing process when the build action fails.

## How does it work?

The GitLab CI/CD pipeline is triggered when a push is made to the repository. The GitLab CI job runs the build script and if it fails, it triggers the healing process. The healing process runs an LLM chain to determine:

1. Where the error is
2. What the error is
3. What the fix is
4. Writes the fix to the file

Then it commits the changes back to the repository.

## Where could this go from here?
This should be treated as an early experimental prototype and currently works on single files only. The next steps could be to:

<!-- checkmark -->
- [ ] Create embeddings of dependencies and use them to determine the correct file to fix (e.g. by passing the relevant context via Vector Similarity Search from a vector DB)
- [ ] Add support for multiple / recursive file detections and errors
- [ ] Add support for retry limits to avoid the infinite loop when no fixes can be found
- [ ] Change workflow to instead of commiting to the branch directly, have the AI commit to a new branch and then create a pull request and assign to the original committer for review
- [ ] Add better test support

## Contributing
If you would like to contribute to this project, please feel free to open a pull request or issue.